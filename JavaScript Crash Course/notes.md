## Variables

**var, let, const**
**var** is globally scoped. Not really used anymore.

always use **const** unless you know you want to change the value


## Datatypes

### Primitive types
- String - single or double quotes
- Number
- Boolean - false or true without quotes
- null
- undefined
- Symbol

**typeof \<variable\>** can be used to test the type

### Template literals
Backticks instead of quotes

```javascript
const text = `My name is ${name}`;
```


## Arrays
```javascript
const numbers = new Array(1,2,3);
const fruits = ['Banana', 'Apple', 'Orange'];
```
One array can hold multiple datatypes.

```javascript
// Add to end
fruits.push('Mango');

// Add to beginning
fruits.unshift('Strawberry');

// Something is Array
Array.isArray(fruits);

// Index of certain value
fruits.indexOf('Orange');
```

## Object literals

```javascript
const person = {
    firstName: 'John'
    lastName: 'Doe'
    age: 30
    hobbies: ['music' , 'movies', 'sports']
    address : {
        street: '50 main st'
        city: 'Boston'
        state: 'MA'
    }
}


// Access values with dot syntax
person.age;
person.hobbies[1];
person.address.city;
```

### Adding properties
```javascript
person.email = 'john@gmail.com';
```

### Destructuring
```javascript
const {firstName, lastName, address: {city}} = person;
console.log(city);
```

### Misc

```javascript
const todos = [
    {
        id: 1,
        text: 'Take out trash',
        isCompleted: true
    },
    {
        id: 2,
        text: 'Meeting with boss',
        isCompleted: true
    },
    {
        id: 3,
        text: 'Dentist appointment',
        isCompleted: false
    }
];

console.log(todos[1].text);

// Converting to JSON
const todoJSON = JSON.stringify(todos);
console.log(todoJSON);
```

## Loops


```javascript
const todos = [
    {
        id: 1,
        text: 'Take out trash',
        isCompleted: true
    },
    {
        id: 2,
        text: 'Meeting with boss',
        isCompleted: true
    },
    {
        id: 3,
        text: 'Dentist appointment',
        isCompleted: false
    }
];

// For
for(let i = 0; i < 10; i++) {
    console.log(i);
}

// While
let i = 0;
while (i < 10) {
    console.log('While Loop Number: ${i}');
    i++;
}

// Looping through Arrays
// Not the best way
for(let i = 0; i < todos.length; i++) {
    console.log(todos[i].text);
}

// For Of
for(let todo of todos) {
    console.log(todo.text)
}

// High order array methods

// forEach
todos.forEach(function (todo) {
    console.log(todo.text)
});

// Map
const toDoText = todos.map(function(todo) {
    return todo.text;
});

console.log(toDoText) // toDoText is an array of texts

// Filter
const todoCompleted = todos.filter(function (todo) {
    return todo.isCompleted == true;
});
console.log(todoCompleted);

// These can be chained
const toDoText = todos.map(function(todo) {
    return todo.text;
}).map(function (todo) {
    return todo.text;
})
```

## Conditionals

### If
```javascript
const x = '10'

// condition is true
if (x == 10) {
    console.log('x is 10')
}

// condition is false. === also matches the datatype
if (x === 10) {
    console.log('x is 10');
} else {
    console.log('X is not 10');
}

```

## Functions

```javascript
function addNums(nu1, num2) {
    console.log(num1 + num2);
}
```

### Arrow functions

```javascript
const addNums = (num1, num2) => {
    return num1 + num2;
}

console.log(addNums(5, 5));
```

## OOP

### Constructor
```javascript
// Constructor function
function Person(firstName, lastName, dob) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.dob = new Date(dob);
}

// Add function to prototype so it's not
// created for each instance of Person
Person.prototype.getBirthYear() = function () {
    return this.dob.getFullYear();
}

const person1 = new Person('John', 'Doe', '4-3-1980')
```

### Classes (Added in ES6) basically syntax sugar
```javascript
class Person {
    constructor(firstName, lastName, dob) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.dob = new Date(dob);
    }
    
    getBirthYear() {
        return.this.dob.getFullYear()
    }
}
```

# DOM