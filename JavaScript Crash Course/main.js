/* Testing
// Selecting DOM elements

// Single element
const formById = document.getElementById('my-form');
const formByQuerySelector = document.querySelector('.container');


// Multiple element
const multipleItems = document.querySelectorAll('.item') // returns a NodeList

// Older ones / querySelector can really do all these anyway
const elementsByClass = document.getElementsByClassName('item') // returns HTMLCollection




// Looping
const items = document.querySelectorAll('.item')
items.forEach((item) => console.log(item));

// Manipulating DOM
const ul = document.querySelector('.items')

// ul.remove()
// ul.lastElementChild.remove()
// ul.firstElementChild.textContent = 'Hello';
// ul.children[1].textContent = 'Brad';
// ul.lastElementChild.innerHTML = '<h4>Hello</h4>';
//
// const btn = document.querySelector('.btn');
// btn.style.background = 'red';

const btn = document.querySelector('.btn');
btn.addEventListener('click', (e) => {
    e.preventDefault()
    document.querySelector('#my-form')
        .style.background = '#ccc'
}); 

*/

const myForm = document.querySelector('#my-form');
const nameInput = document.querySelector('#name');
const emailInput = document.querySelector('#email');
const msg = document.querySelector('.msg');
const userList = document.querySelector('#users');

myForm.addEventListener('submit', onSubmit);

function onSubmit(e) {
    e.preventDefault();
    
    if(nameInput.value === '' || emailInput.value === '') {
        msg.classList.add('error')
        msg.innerHTML = 'Please enter all fields';
        
        setTimeout(() => {
            msg.innerHTML = '';
            msg.classList.remove('error');
        }, 3000);
    } else {
        const li = document.createElement('li');
        li.appendChild(document.createTextNode(`
        ${nameInput.value} : ${emailInput.value} `));
        
        userList.appendChild(li);
        
        // Clear the fields
        nameInput.value = '';
        emailInput.value = '';
    }
}