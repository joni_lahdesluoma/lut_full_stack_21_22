import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  authToken: any;
  user: any;

  constructor(
    private httpClient: HttpClient,
    private jwtHelper: JwtHelperService) {}

  registerUser(user: any) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        responseType: 'json',
      }),
    };
    return this.httpClient.post<any>(
      'http://localhost:3000/users/register',
      user,
      httpOptions
    );
  }

  authenticateUser(user: any) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        responseType: 'json',
      }),
    };

    return this.httpClient.post<any>(
      'http://localhost:3000/users/authenticate',
      user,
      httpOptions
    );
  }

  getProfile() {
    this.loadToken();
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        responseType: 'json',
        Authorization: this.authToken,
      }),
    };

    return this.httpClient.get<any>(
      'http://localhost:3000/users/profile',
      httpOptions
    );
  }

  storeUserData(token: string, user: Object) {
    localStorage.setItem('id_token', token);
    localStorage.setItem('user', JSON.stringify(user));
    this.authToken = token;
    this.user = user;
  }

  loadToken() {
    const token = localStorage.getItem('id_token');
    this.authToken = token;
  }

  logout() {
    this.authToken = null;
    this.user = null;
    localStorage.clear();
  }

  loggedIn() {
    return !this.jwtHelper.isTokenExpired(this.authToken) 
  }
}
